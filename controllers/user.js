var mongoose = require('mongoose');
var User = mongoose.model('User')

exports.add = function(req, res) {
 console.log('POST');
 console.log(req.body);
 var user = new User({
 name: req.body.name,
 lastName: req.body.lastName,
 email: req.body.email,
 genre: req.body.genre
 });
 user.save(function(err, user) {
    console.log(err.message);
    if(err){
        var codeError = 500;
        if(err.message === 'Validation failed'){
            codeError = 401;
        }
        return res.send(codeError, {message: err.message});
    } 
    res.status(200).jsonp(user);
 });
};